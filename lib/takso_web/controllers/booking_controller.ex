defmodule TaksoWeb.BookingController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias Takso.{Repo,Union.Taxi}

  def index(conn, _params) do
    render conn, "index.html"
  end

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, params) do
    IO.puts "====================================="
    IO.inspect params
    IO.puts "====================================="

    query = from t in Taxi, 
            where: t.status == "available", 
            select: t

    available_taxis = Repo.all(query)

    if (length(available_taxis) > 0) do
      conn
      |> put_flash(:info, "Your taxi will arrive in 10 minutes")
      |> redirect(to: booking_path(conn, :index))
    else
      conn
      |> put_flash(:error, "At present, there is no taxi available!")
      |> redirect(to: booking_path(conn, :index))
    end
  end
end
